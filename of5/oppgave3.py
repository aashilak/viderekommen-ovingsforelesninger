#I denne oppgaven skal vi lage en forenklet versjon av yatzy
#Først trenger vi en funksjon som triller 5 terninger for oss.*

#a) Lag en funksjon, `roll_dice`,  som lager en liste som inneholder
# 5 elementer der hvert element har en tilfeldig tall-verdi mellom 1 og 6.
#Hint: import random, random.randint(1, 6) 







#Videre trenger vi en funksjon for å telle antall forekomster
#  av en gitt verdi, f.eks. enere og toere*
#b) Lag en funksjon som tar inn listen med tall og et heltall mellom 1 og 6,
#  og returnerer antallet terninger som har den heltallsverdien 






#c) Til slutt vil vi kunne spille hele den første delen av et Yatzy-spill

#Lag en funksjon som kaster terningene seks ganger, og gir poeng for antall 
# enere i det første kastet, toere i det andre, osv. 
#- Print poeng for hvert kast
#- Returner summen